#!/usr/bin/env bash

get_latest_static_url() {
  curl -s -D - https://aka.ms/downloadazcopy-v10-linux | awk '{if($1 ~ /Location/){printf "%s",$2}}' | tr -d '\r'
}

replace_static_url() {
  local new_url="$1"
  local file="$2"

  new_url=${new_url//amd64/\$\{TARGETARCH\}}
  sed -i "s|ARG AZCOPY_STATIC_URL=\".*\"|ARG AZCOPY_STATIC_URL=\"${new_url}\"|" "${file}"
}

url=$(get_latest_static_url)
replace_static_url "${url}" gitlab-toolbox/Dockerfile
replace_static_url "${url}" gitlab-toolbox/Dockerfile.build.ubi
