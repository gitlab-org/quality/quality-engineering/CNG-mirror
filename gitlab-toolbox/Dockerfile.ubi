## FINAL IMAGE ##
ARG RAILS_IMAGE=
ARG UBI_IMAGE=registry.access.redhat.com/ubi9/ubi-minimal:9.4
ARG UBI_IMAGE_MICRO=registry.access.redhat.com/ubi9/ubi-micro:9.4

FROM ${UBI_IMAGE_MICRO} as target

FROM ${UBI_IMAGE} as build
ARG GITLAB_USER=git
ARG UID=1000
ARG DNF_OPTS
ARG DNF_OPTS_ROOT
ARG DNF_INSTALL_ROOT=/install-root
ENV LIBDIR ${LIBDIR:-"/usr/lib64"}

RUN mkdir -p ${DNF_INSTALL_ROOT}
COPY --from=target / ${DNF_INSTALL_ROOT}/

ADD gitlab-toolbox-ee.tar.gz ${DNF_INSTALL_ROOT}/
ADD gitlab-python.tar.gz ${DNF_INSTALL_ROOT}/

COPY scripts/bin/* ${DNF_INSTALL_ROOT}/usr/local/bin/
COPY scripts/lib/* ${DNF_INSTALL_ROOT}/${LIBDIR}/ruby/vendor_ruby/

RUN microdnf update -y \
    && microdnf ${DNF_OPTS} install --nodocs --best --assumeyes --setopt=install_weak_deps=0 shadow-utils \
    && adduser -u ${UID} -m ${GITLAB_USER} -R ${DNF_INSTALL_ROOT}/

RUN microdnf ${DNF_OPTS} ${DNF_OPTS_ROOT} install --best --assumeyes --nodocs --setopt=install_weak_deps=0 openssl jq \
    && microdnf ${DNF_OPTS_ROOT} clean all \
    && mkdir -p ${DNF_INSTALL_ROOT}/scripts ${DNF_INSTALL_ROOT}/home/${GITLAB_USER} ${DNF_INSTALL_ROOT}/var/log/gitlab \
    && chown -R ${UID}:0 ${DNF_INSTALL_ROOT}/scripts ${DNF_INSTALL_ROOT}/home/${GITLAB_USER} ${DNF_INSTALL_ROOT}/var/log/gitlab \
    && chmod -R g=u ${DNF_INSTALL_ROOT}/scripts ${DNF_INSTALL_ROOT}/home/${GITLAB_USER} ${DNF_INSTALL_ROOT}/var/log/gitlab

FROM ${RAILS_IMAGE}

ARG GITLAB_VERSION
ARG GITLAB_USER=git
ARG UID=1000
ARG DNF_INSTALL_ROOT=/install-root

ENV LIBDIR ${LIBDIR:-"/usr/lib64"}
ARG FIPS_MODE=0

LABEL source="https://gitlab.com/gitlab-org/build/CNG/-/tree/master/gitlab-toolbox" \
      name="GitLab Toolbox" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITLAB_VERSION} \
      release=${GITLAB_VERSION} \
      summary="Toolbox is an entry point for interaction with other containers in the cluster." \
      description="Toolbox is an entry point for interaction with other containers in the cluster. It contains scripts for running Rake tasks, backup, restore, and tools to interact with object storage."

COPY --from=build ${DNF_INSTALL_ROOT}/ /

## Hardening: CIS L1 SCAP
RUN --mount=type=bind,rw,from=hardening,source=/,target=/hardening \
    set -ex; for f in /hardening/*.sh; do sh "$f"; done

# Declare /var/log volume after initial log files
# are written to the perms can be fixed
VOLUME /var/log

USER ${UID}

ENTRYPOINT ["/usr/local/bin/entrypoint-ubi.sh"]
