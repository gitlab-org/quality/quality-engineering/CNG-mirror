ARG CI_REGISTRY_IMAGE="registry.gitlab.com/gitlab-org/build/cng"
ARG FROM_IMAGE="$CI_REGISTRY_IMAGE/gitlab-go"
ARG GO_TAG="master"
ARG TAG="master"
ARG GITLAB_BASE_IMAGE="${CI_REGISTRY_IMAGE}/gitlab-base:${TAG}"

FROM --platform=${TARGETPLATFORM} ${FROM_IMAGE}:${GO_TAG} as builder

ARG BUILD_DIR=/tmp/build
ARG DOCKER_BUILDTAGS="include_oss include_gcs continuous_profiler_stackdriver"
ARG ZOEKT_INDEXER_VERSION="v0.8.4-d6319"
ARG ZOEKT_INDEXER_REPO="https://gitlab.com/gitlab-org/gitlab-zoekt-indexer.git"
ARG CTAGS_VERSION="v6.0.0"
ARG GOPATH=/go
ARG REGISTRY_SOURCE_PATH=${GOPATH}/src/github.com/docker/distribution
ARG GITLAB_BASE_IMAGE

ENV GOTOOLCHAIN=local

RUN buildDeps=' \
  git pkg-config build-essential autoconf automake libjansson-dev' \
  && apt-get update \
  && apt-get install -y --no-install-recommends $buildDeps

# install universal-ctags from source
RUN mkdir -p ${BUILD_DIR} \
  && cd ${BUILD_DIR} \
  && git clone https://github.com/universal-ctags/ctags.git "ctags-$CTAGS_VERSION" \
  && cd "ctags-$CTAGS_VERSION" \
  && git reset --hard $CTAGS_VERSION \
  && ./autogen.sh \
  && ./configure --program-prefix=universal- --enable-json \
  && make -j$(nproc) \
  && mv ./ctags /usr/local/bin/universal-ctags

RUN mkdir -p ${BUILD_DIR} \
  && cd ${BUILD_DIR} \
  && git clone $ZOEKT_INDEXER_REPO "gitlab-zoekt-indexer-$ZOEKT_INDEXER_VERSION" \
  && cd "gitlab-zoekt-indexer-$ZOEKT_INDEXER_VERSION" \
  && git reset --hard $ZOEKT_INDEXER_VERSION \
  && CGO_ENABLED=0 make build \
  && mv ./bin/gitlab-zoekt-indexer /usr/local/bin/gitlab-zoekt-indexer

## FINAL IMAGE ##

FROM --platform=${TARGETPLATFORM} ${GITLAB_BASE_IMAGE}

ARG GITLAB_USER=git

RUN apt-get update \
  && apt-get install -y --no-install-recommends libseccomp2 libjansson4 libyaml-0-2 libxml2 \
  && rm -rf /var/lib/apt/lists/* \
  && adduser --disabled-password --gecos 'GitLab' ${GITLAB_USER} \
  && mkdir -p /data/index \
  && chown -R $GITLAB_USER:$GITLAB_USER /data/index

VOLUME ["/data/index"]

COPY --from=builder /usr/local/bin/gitlab-zoekt-indexer /bin/
COPY --from=builder /usr/local/bin/universal-ctags /bin/
COPY scripts/ /scripts/

USER $GITLAB_USER:$GITLAB_USER

CMD ["/scripts/process-wrapper"]

HEALTHCHECK --interval=30s --timeout=30s --retries=5 CMD /scripts/healthcheck
