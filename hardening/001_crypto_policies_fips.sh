#!/bin/sh
set -e

if [ "$FIPS_MODE" -eq 1 ]; then

(>&2 echo "Configuration: Setting system crypto-policies to FIPS")

if [ ! -d /etc/crypto-policies ]; then
  (>&2 echo "Notice: Configured for FIPS, but no crypto-policies found")
  exit
fi

## Method derived from comparing host before and after
## fips-mode-setup scripts.
## Details in https://gitlab.com/gitlab-org/build/CNG/-/issues/779

printf '# FIPS enabled' > /etc/system-fips
printf 'FIPS' > /etc/crypto-policies/config
printf 'FIPS' > /etc/crypto-policies/state/current

for config in /usr/share/crypto-policies/FIPS/* ; do
    file=$(basename "$config")
    file="${file%.txt}.config"
    ln -sf "$config" "/etc/crypto-policies/back-ends/$file"
done

fi
