ARG BUILD_IMAGE=

FROM ${BUILD_IMAGE}

ARG GITALY_SERVER_VERSION=master
ARG GITALY_GIT_REPO_URL
ARG NAMESPACE=gitlab-org
ARG PROJECT=gitaly
ARG API_URL=
ARG API_TOKEN=
ARG FIPS_MODE=
ARG GIT_FILTER_REPO_VERSION="2.45.0"

ARG BUNDLE_OPTIONS="--jobs 4"

ENV GOTOOLCHAIN=local

ADD gitlab-go.tar.gz /
ADD gitlab-python.tar.gz /

ENV LANG=C.UTF-8
ENV PRIVATE_TOKEN=${API_TOKEN}
ENV LIBDIR ${LIBDIR:-"/usr/lib64"}

COPY shared/build-scripts/ /build-scripts

RUN mkdir /assets \
    && ln -sf /usr/local/go/bin/* /usr/local/bin \
    && pip3 install "git-filter-repo==${GIT_FILTER_REPO_VERSION}" \
    && pip3 cache purge \
    && find /usr/local/lib/python3.9 -name '__pycache__' -type d -print -exec rm -r {} + \
    && /gitlab-fetch \
        "${API_URL}" \
        "${NAMESPACE}" \
        "${PROJECT}" \
        "${GITALY_SERVER_VERSION}" \
    && cd ${PROJECT}-${GITALY_SERVER_VERSION} \
    && install -D LICENSE /licenses/GitLab.txt \
    && if [ -n "${GITALY_GIT_REPO_URL}" ]; then export GIT_REPO_URL="${GITALY_GIT_REPO_URL}" ; fi \
    && FIPS_MODE=${FIPS_MODE} make install WITH_BUNDLED_GIT=YesPlease \
    && cp -R --parents \
      /usr/local/lib/python3.9/site-packages \
      /usr/local/bin/gitaly* \
      /usr/local/bin/praefect \
      /licenses \
      /assets
